package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            
            String human = getHumanChoice();
            String computer = getComputerChoice();

            String choices = "Human chose "+human+", computer chose "+computer+".";

            if (computer.equals(human))
            {
                System.out.println(choices + " It's a tie!");
            }
            else if (isHumanWinner(human, computer)) {
                System.out.println(choices + " Human wins!");
                humanScore += 1;
            }
            else {
                System.out.println(choices + " Computer wins!");
                computerScore += 1;
            }

            System.out.println("Score: human "+humanScore+", computer " + computerScore);

            if (readInput("Do you wish to continue playing? (y/n)?").equals("n"))
            {
                System.out.println("Bye bye :)");
                return;
            }
        }
    }

    private Boolean isHumanWinner(String human, String computer)
    {
        return human.equals("rock") && computer.equals("scissors")
            || human.equals("paper") && computer.equals("rock")
            || human.equals("scissors") && computer.equals("paper");
    }

    private String getHumanChoice()
    {
        while (true) {
            String choice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (rpsChoices.contains(choice))
                return choice;
            System.out.println("I do not understand " + choice + ". Could you try again?");
        }
    }

    private String getComputerChoice()
    {
        java.util.Random random = new java.util.Random();
        return rpsChoices.get(random.nextInt(rpsChoices.size()));
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
